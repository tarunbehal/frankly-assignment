# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='NewsItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=250)),
                ('url', models.CharField(max_length=250, null=True, blank=True)),
                ('hacker_news_url', models.CharField(max_length=250, null=True, blank=True)),
                ('posted_on', models.DateTimeField(null=True, blank=True)),
                ('upvote', models.PositiveIntegerField(default=0)),
                ('comment', models.PositiveIntegerField(default=0)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('deleted_news', models.ManyToManyField(related_name=b'deleted_news', to='sitebase.NewsItem', blank=True)),
                ('read_news', models.ManyToManyField(related_name=b'read_news', to='sitebase.NewsItem', blank=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
