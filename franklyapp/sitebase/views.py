from django.shortcuts import render
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db.models import Q
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from django.utils import timezone
from django.views.generic.base import TemplateView
from django.utils.decorators import method_decorator
from django.template.loader import get_template
from django.template import Context
from datetime import datetime, timedelta
from django.views.decorators.csrf import ensure_csrf_cookie
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.shortcuts import get_object_or_404
from .models import NewsItem
from django.core import management
from franklyapp.sitebase.management.commands import pull_news


def build_action_button(news, user):
    template = get_template('_build_action_buttons.html')
    context = Context({'news': news, 'user': user})
    return "{0}".format(template.render(context))


class NewsListJson(BaseDatatableView):
    model = NewsItem

    # define the columns that will be returned
    columns = ['id', 'title', 'url', 'hacker_news_url',
               'posted_on', 'upvote', 'comment', 'action']
    order_columns = ['id', 'title', 'url', 'hacker_news_url',
                     'posted_on', 'upvote', 'comment', '', '']
    max_display_length = 100

    def get_initial_queryset(self):
        delted_news = self.request.user.profile.deleted_news.all()
        return self.model.objects.filter(active=True).exclude(id__in=delted_news).order_by('-id')

    def render_column(self, row, column):
        if column == 'action':
            return build_action_button(row, self.request.user)
        else:
            return super(NewsListJson, self).render_column(row, column)

    def filter_queryset(self, qs):
        search = self.request.GET.get('sSearch', None)
        if search:
            qs = qs.filter(
                Q(title__istartswith=search) | Q(title__icontains=search))
        return qs


class Home(TemplateView):

    template_name = "dashboard.html"
    http_method_names = ['get']

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(Home, self).dispatch(*args, **kwargs)


@login_required
def news_handler(request):
    news_id = request.POST.get('id')
    action = request.POST.get('op')
    res = {'status': 'fail'}
    if news_id and action:
        news = get_object_or_404(NewsItem, id=news_id)
        if action == 'delete':
            request.user.profile.deleted_news.add(news)
            res['status'] = 'ok'
        elif action == 'read':
            request.user.profile.read_news.add(news)
            res['status'] = 'ok'
    return JsonResponse(res)
