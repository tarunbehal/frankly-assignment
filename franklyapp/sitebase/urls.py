from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', Home.as_view(), name='home'),
    url(r'^news/datatable/data/$',
        NewsListJson.as_view(), name='news_list_json'),
    url(r'^news/action/$',
        news_handler, name='news_handler'),
]
