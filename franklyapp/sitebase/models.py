# @author: Tarun Behal
# @date: Sep 12,2015

from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class ItemBase(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class NewsItem(ItemBase):

    title = models.CharField(max_length=250)
    url = models.CharField(max_length=250, blank=True, null=True)
    hacker_news_url = models.CharField(max_length=250, blank=True, null=True)
    posted_on = models.DateTimeField(blank=True, null=True)
    upvote = models.PositiveIntegerField(default=0)
    comment = models.PositiveIntegerField(default=0)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.title


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    read_news = models.ManyToManyField(
        NewsItem, related_name='read_news', blank=True)
    deleted_news = models.ManyToManyField(
        NewsItem, related_name='deleted_news', blank=True)


User.profile = property(
    lambda u: UserProfile.objects.get_or_create(user=u)[0])
