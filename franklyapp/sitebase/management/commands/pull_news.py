# @author: Tarun Behal
# @date: Sep 12,2015

import os
import sys

from django.core.management.base import BaseCommand
from scrapy import cmdline
from django.conf import settings


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        self.start_pull_news()

    def start_pull_news(self):
        sys.path.insert(0, settings.SCRAPY_PATH)
        os.environ['SCRAPY_SETTINGS_MODULE'] = '{}.settings'.format(
            settings.SCRAPY_PROJECT)
        cmdline.execute(
            ["scrapy", "crawl", "news"]
        )
