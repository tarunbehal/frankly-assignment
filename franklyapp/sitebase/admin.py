# @author: Tarun Behal
# @date: Sep 12,2015

from django.contrib import admin
from .models import NewsItem


class NewsItemAdmin(admin.ModelAdmin):
    search_fields = ("title",)

admin.site.register(NewsItem, NewsItemAdmin)
