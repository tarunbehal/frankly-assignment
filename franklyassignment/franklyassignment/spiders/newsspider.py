# @author: Tarun Behal
# @date: Sep 12,2015

import scrapy
from itertools import imap
import re
import datetime
import parsedatetime

from franklyassignment.items import ScrapedNewsItem

cal = parsedatetime.Calendar()


def grouper(n, iterable):
    args = [iter(iterable)] * n
    return imap(None, *args)


class NewsSpider(scrapy.Spider):
    name = "news"
    allowed_domains = ["ycombinator.com"]
    abs_url = 'https://news.ycombinator.com/'
    start_urls = [
        'https://news.ycombinator.com/news?p={}'.format(page)
        for page in xrange(1, 4)
    ]

    def extract_text(self, node):
        if not node:
            return ''
        extracted_list = [
            x.strip() for x in node.extract() if len(x.strip()) > 0]
        if not extracted_list:
            return ''
        return ' '.join(extracted_list)

    def parse(self, response):
        news_items = response.xpath('//*[@id="hnmain"]/tr[3]/td/table/tr')
        for news in grouper(3, news_items):
            news_item = ScrapedNewsItem()
            news_item['title'] = news[0].xpath(
                'td[3]/a/text()').extract()[0].strip()
            news_item['url'] = news[0].xpath(
                'td[3]/a/@href').extract()[0].strip()
            hacker_news_url_text = news[1].xpath('td[2]/a[3]/@href').extract()
            if hacker_news_url_text:
                news_item['hacker_news_url'] = "{0}{1}".format(
                    self.abs_url, hacker_news_url_text[0]
                )
            upvotes_text = news[1].xpath('td[2]/span/text()').extract()
            try:
                upvotes = re.findall('\d+', upvotes_text[0].strip())[0]
            except IndexError:
                upvotes = 0
            comment_text = news[1].xpath('td[2]/a[3]/text()').extract()
            try:
                comment = re.findall('\d+', comment_text[0].strip())[0]
            except IndexError:
                comment = 0
            news_item['upvote'] = upvotes
            news_item['comment'] = comment
            posted_on_text = news[1].xpath(
                'td[2]/a[2]/text()').extract()
            if not posted_on_text:
                posted_on_text = news[1].xpath(
                    'td[2]/text()').extract()

            if posted_on_text:
                datetime_obj, _ = cal.parseDT(datetimeString=posted_on_text[0])
                if datetime_obj:
                    news_item['posted_on'] = str(datetime_obj)
                else:
                    news_item['posted_on'] = str(datetime.dateime.now())
            yield news_item
