# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
# @author: Tarun Behal
# @date: Sep 12,2015

from franklyapp.sitebase.models import NewsItem


class FranklyassignmentPipeline(object):

    def process_item(self, item, spider):
        ex_news = NewsItem.objects.filter(
            title=item['title'],
            url=item['url']
        ).first()
        if ex_news:
            ex_news.upvote = item['upvote']
            ex_news.comment = item['comment']
            ex_news.posted_on = item['posted_on']
            if item.get('hacker_news_url', None):
                ex_news.hacker_news_url = item['hacker_news_url']
            ex_news.save()
        else:
            item.save()
        return item
